//
//  secondViewController.h
//  FlipAnimation
//
//  Created by Amit Kumar on 2016-11-22.
//  Copyright © 2016 Saurabh. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LetsGoDo <NSObject>

-(void)letsGomethod;

@end


@interface secondViewController : UIViewController
@property(weak)id<LetsGoDo>delegate;

@end
