//
//  ViewController.m
//  FlipAnimation
//
//  Created by Amit Kumar on 2016-11-21.
//  Copyright © 2016 Saurabh. All rights reserved.
//

#import "ViewController.h"

#import "CETurnAnimationController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "CEBaseInteractionController.h"


#import "ZeroViewController.h"
#import "secondViewController.h"
#import "thireViewController.h"
#import "firstViewController.h"

@interface ViewController ()<LetsGoDo,zeroDelegate>
{
    
    NSArray* _colors;
    bool a ;
    UINavigationController *nav;

}

@property (weak, nonatomic) IBOutlet UIView *animateView;
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;

@end
static int colorIndex = 0;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBarHidden = YES;
    
    ZeroViewController *zero = nav.viewControllers[0];
    zero.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)buttonAction:(id)sender {


//    {
//        if (a == NO) {
//            [UIView transitionFromView:self.firstView toView:self.secondView
//                              duration:1.0
//                               options:UIViewAnimationOptionTransitionFlipFromLeft
//                            completion:NULL];
//            a = YES; // a = !a;
//        }
//        else {
//            [UIView transitionFromView:self.secondView toView:self.firstView
//                              duration:1.0
//                               options:UIViewAnimationOptionTransitionFlipFromLeft
//                            completion:NULL];
//            a = NO; // a = !a;
//        }
//
//    }}


}

-(void)letsGomethod
{
    UIStoryboard * storyboard = self.storyboard;
    thireViewController * third = [storyboard instantiateViewControllerWithIdentifier: @"third"];
    [UIView transitionWithView:nav.view duration:0.4 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        [nav setViewControllers:@[third] animated:YES];
    } completion:nil];
    
}

-(void)gotoSecondViewController
{
    UIStoryboard * storyboard = self.storyboard;
    secondViewController * second = [storyboard instantiateViewControllerWithIdentifier: @"second"];
    second.delegate = self;
    [UIView transitionWithView:nav.view duration:0.4 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        [nav setViewControllers:@[second] animated:YES];
    } completion:nil];
    

}



- (IBAction)previousViewController:(id)sender {
    UIStoryboard * storyboard = self.storyboard;
    firstViewController * first = [storyboard instantiateViewControllerWithIdentifier: @"first"];
    [UIView transitionWithView:nav.view duration:0.4 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        [nav setViewControllers:@[first] animated:YES];
        
        
    } completion:nil];

}
- (IBAction)nextViewController:(id)sender {

    UIStoryboard * storyboard = self.storyboard;
    thireViewController * third = [storyboard instantiateViewControllerWithIdentifier: @"third"];
    
    [UIView transitionWithView:nav.view duration:0.4 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        [nav setViewControllers:@[third] animated:YES];
        
        
    } completion:nil];
    



}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EmbedViewCSegue"]) {
        nav = segue.destinationViewController;
    
    }
}


@end

