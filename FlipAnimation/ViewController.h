//
//  ViewController.h
//  FlipAnimation
//
//  Created by Amit Kumar on 2016-11-21.
//  Copyright © 2016 Saurabh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEReversibleAnimationController.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) CEReversibleAnimationController *settingsAnimationController;

@end

