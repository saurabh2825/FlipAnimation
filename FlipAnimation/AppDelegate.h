//
//  AppDelegate.h
//  FlipAnimation
//
//  Created by Amit Kumar on 2016-11-21.
//  Copyright © 2016 Saurabh. All rights reserved.
//

#import <UIKit/UIKit.h>
#define AppDelegateAccessor ((AppDelegate *)[[UIApplication sharedApplication] delegate])
@class CEReversibleAnimationController, CEBaseInteractionController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CEReversibleAnimationController *settingsAnimationController;
@property (strong, nonatomic) CEReversibleAnimationController *navigationControllerAnimationController;
@property (strong, nonatomic) CEBaseInteractionController *navigationControllerInteractionController;
@property (strong, nonatomic) CEBaseInteractionController *settingsInteractionController;


@end

