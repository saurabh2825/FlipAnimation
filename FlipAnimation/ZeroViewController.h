//
//  ZeroViewController.h
//  FlipAnimation
//
//  Created by Amit Kumar on 2016-11-22.
//  Copyright © 2016 Saurabh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol zeroDelegate <NSObject>

-(void)gotoSecondViewController;

@end

@interface ZeroViewController : UIViewController

@property(weak)id<zeroDelegate>delegate;



@end
